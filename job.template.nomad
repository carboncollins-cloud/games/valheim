job "game-valheim" {
  name = "Game Server (Valheim)"
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "game-servers"

  vault {
    policies = ["job-game-valheim"]
  }

  group "valheim" {
    count = 1

    affinity {
      attribute = "${attr.unique.hostname} "
      value     = "proxima-g"
      weight    = 75
    }

    volume "valheim-data" {
      type = "csi"
      source = "valheim-data"
      read_only = false

      attachment_mode = "file-system"
      access_mode = "single-node-writer"
    }

    volume "axion-backup-valheim" {
      type = "host"
      read_only = false
      source = "axion-backup-valheim"
    }

    network {
      mode = "bridge"

      port "valheim1" {
        to = 2456
        static = 2456
      }
      port "valheim2" {
        to = 2457
        static = 2457
      }
      port "valheim3" {
        to = 2458
        static = 2458
      }
    }

    task "valheim" {
      driver = "docker"

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      // this should be an affinity when edge router can proxy again
      constraint {
        attribute = "${attr.unique.hostname}"
        value = "proxima-g"
        // weight = 50
      }

      volume_mount {
        volume = "valheim-data"
        destination = "/home/steam/.config/unity3d/IronGate/Valheim"
        read_only = false
      }

      volume_mount {
        volume = "axion-backup-valheim"
        destination = "/home/steam/backups"
        read_only = false
      }

      config {
        image = "mbround18/valheim:latest"

        ports = ["valheim1", "valheim2", "valheim3"]

        volumes = [
          "local/valheim/server:/home/steam/valheim"
        ]
      }

      template {
        data = <<EOH
[[ fileContents "./config/valheim.template.env" ]]
        EOH

        destination = "secrets/valheim.env"
        env = true
      }

      env {
        TZ="Europe/Stockholm"
        PUID=[[ .defaultUserId ]]
        PGID=[[ .defaultGroupId ]]
      }

      resources {
        // cores = 1
        cpu = 4200
        memory = 4096
        memory_max = 6144
      }

      service {
        name = "valheim"
        port = "valheim1"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
